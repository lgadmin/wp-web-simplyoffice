<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	<footer id="colophon" class="site-footer">
		<div id="site-footer" class="bg-image-alpha clearfix pb-lg pt-lg">
			<div class="footer-brand">
				<?php
					if(shortcode_exists('lg-site-logo')){
						echo do_shortcode('[lg-site-logo]');
					}else{
						the_custom_logo();
					} 
			 	?>
				<div class="small">
					<!-- We need the footer blurb option -->
					<?php echo get_field('footer_blurb', 'option'); ?>
					<p>Simply Office is a full-service office space in Vancouver that provides both physical and virtual rental opportunities to businesses in Greater Vancouver. With sweeping views of the Vancouver skyline, outfitted boardrooms, reception area, state-of-the-art telephone system and business equipment, fully fitted kitchen and administrative staff, all you have to do is move in!</p>
						
				</div>
			</div>
			<div class="footer-utility"><?php get_template_part("/templates/template-parts/address-card"); ?></div>
            <div class="nav-footer"><?php get_template_part("/templates/template-parts/nav-footer"); ?></div>
			<div class="blog-feed">
				<h2 class="h4">Stay Conected</h2>
				<?php echo do_shortcode('[lg-social-media]'); ?>
			</div>
		</div>
		<div id="site-legal" class="bg-gray-base pb-lg clearfix">
			<div class="site-info"><?php get_template_part("/templates/template-parts/site-info"); ?></div>
			<div class="site-longevity"> <?php get_template_part("/templates/template-parts/site-footer-longevity"); ?> </div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
