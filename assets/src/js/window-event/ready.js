// Windows Ready Handler

function headerScrollTo(toElement, settings){
	if($(toElement)[0]){
		var currentTop = $(window).scrollTop();
		var elementTop = $(toElement).offset().top;

		console.log(settings.duration);

		$('html, body').stop().animate({
	        scrollTop: elementTop + settings.offset
	    }, settings.duration);
	}
}

function enable_office_page_popup(image, building, room_type){

		var element = $('.offices-pop-up');

		element.find('.left >img').attr('src', image);
		element.find('.left .building').text(building);
		element.find('.form-building input').val(building);
		element.find('.left .room-type').text(room_type);
		element.find('.form-room-type input').val(room_type);

		element.fadeIn(800);

}

function pageTo(domElement, page, isScroll){

	var animationDuration = 800;

	domElement.find('.page:visible').css('display', 'none');
	domElement.find('.page[value=' + page + ']').fadeIn(animationDuration);
	domElement.siblings('nav').find('ul li a[href="'+page+'"]').addClass('active').closest('li').siblings().find('a').removeClass('active');

	if(isScroll){
		var settings = {
	        duration: 1000,
	        offset: 0
	    };

	    var scrollTo = domElement.closest('.building');

	    headerScrollTo(scrollTo, settings);
	}
}



if($('.offices')[0]){

	$('.offices .office-list').each(function(){
		//Setting Variables
		var officeToShow = 6;

		var currentElement = $(this);

		var officeLength = currentElement.find('.single-office').length;
		var pagination = Math.ceil(officeLength / officeToShow);

		var newHtml = '';

		for(var i = 0; i < pagination; i++){
			var newElement = '<div class="page" value="' + (i+1) + '"><div>';
			var children = currentElement.children();

			for(var j = i * officeToShow; j < (i+1) * officeToShow; j++){
				if(children[j]){
					newElement += '<div class="single-office">' + $(children[j]).html() + '</div>'
				}
			}
			newElement += '</div></div>';
			newHtml += newElement;
		}


		var paginationHtml = '<nav aria-label="Page navigation"><ul class="pagination">';
		for (var i = 0; i < pagination; i++) {
			paginationHtml += '<li><a href="' + (i+1) + '">' + (i+1) + '</a></li>';
		}
		paginationHtml += '</ul></nav>';

		currentElement.html(newHtml);
		currentElement.closest('.building').append(paginationHtml).find('.pagination li a').on('click', function(e){
			e.preventDefault();
			$(this).addClass('active').siblings().removeClass('active');
			pageTo(currentElement, $(this).attr('href'), true);
		});

		pageTo(currentElement, 1, false);
	});


	$('.offices').on('click', '.cta', function(e){
		e.preventDefault();
		var building = $(this).closest('.building').find('>h2').text();
		var image = $(this).siblings('.thumbnail').find('img').attr('src');
		var room_type = $(this).siblings('.description').find('>h3').text();

		enable_office_page_popup(image, building, room_type);
	});

	$('.offices-pop-up .gform_footer').append('<input class="gform_button button button_cancel" value="Cancel">').on('click', function(){
		$('.offices-pop-up').fadeOut(800);
	});

}
