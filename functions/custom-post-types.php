<?php

//Custom Post Types
function create_post_type() {

    // LOCATION
    register_post_type( 'location',
        array(
          'labels' => array(
            'name' => __( 'Locations' ),
            'singular_name' => __( 'Location' )
          ),
          'public' => false,
          'has_archive' => false,
          'menu_icon'   => 'dashicons-location',
          'show_ui'     => true,
          'show_in_menu'    => 'simply_office',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

    // BUILDING
    register_post_type( 'building',
        array(
          'labels' => array(
            'name' => __( 'Buildings' ),
            'singular_name' => __( 'Building' )
          ),
          'public' => false,
          'has_archive' => false,
          'menu_icon'   => 'dashicons-location',
          'show_ui'     => true,
          'show_in_menu'    => 'simply_office',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

    // OFFICE
    register_post_type( 'office',
        array(
          'labels' => array(
            'name' => __( 'Offices' ),
            'singular_name' => __( 'Office' )
          ),
          'public' => true,
          'has_archive' => false,
          'menu_icon'   => 'dashicons-location',
          'show_ui'     => true,
          'show_in_menu'    => 'simply_office',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

    // VIRTUAL PACKAGES
    register_post_type( 'virtual_package',
        array(
          'labels' => array(
            'name' => __( 'Virtual Packages' ),
            'singular_name' => __( 'Virtual Package' )
          ),
          'public' => true,
          'has_archive' => false,
          'menu_icon'   => 'dashicons-location',
          'show_ui'     => true,
          'show_in_menu'    => 'simply_office',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

    // TEAM
    register_post_type( 'member',
        array(
          'labels' => array(
            'name' => __( 'Team Members' ),
            'singular_name' => __( 'Team Member' )
          ),
          'public' => true,
          'has_archive' => true,
          'menu_icon'   => 'dashicons-location',
          'show_ui'     => true,
          'show_in_menu'    => 'simply_office',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

}
add_action( 'init', 'create_post_type' );

?>