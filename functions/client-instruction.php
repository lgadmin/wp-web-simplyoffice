<style>
	ul{
		list-style:default;
		padding-left:20px;
	}

	span.nav{
		color: green;
		font-weight: 600;
	}
</style>

<div>
	<h1>Client Instruction</h1>

	<div style="padding: 15px; background-color:#fff;">
		
		<div>
			<h2>Site General</h2>
			<ul>
				<li><b>Logo:</b> <span class="nav">Dashboard -> LG Theme -> Site Settings -> Logo</span></li>
				<li><b>Social Media:</b> <span class="nav">Dashboard -> LG Theme -> Social Media</span></li>
				<li><b>Contact Info:</b> <span class="nav">Dashboard-> LG Theme -> Contact</span></li>
				<li><b>Tracking Scripts:</b> <span class="nav">Dashboard -> LG Theme -> Analytics Tracking</span> (Google tag manager setup under Longevity Account, No code injection to the header)</li>
			</ul>
		</div>

		<hr>

		<div>
			<h2>Content Update</h2>
			<ul>
				<li><b>Home:</b> <span class="nav">Dashboard -> Pages -> Home</span></li>
				<li><b>About:</b>
					<ul style="margin:15px 0;">
						<li><b>About:</b> Change copy at <span class="nav">Dashboard -> Pages -> About</span>. Add Team Member at <span class="nav">Dashboard -> Simply Office -> Team Member</span></li>
						<li><b>Testimonial:</b> <span class="nav">Dashboard -> Testimonials</span> </li>
					</ul>
				</li>
				<li><b>Office:</b> Change Copy at <span class="nav">Dashboard -> Pages -> Offices</span>, Update office information at <span class="nav">Dashboard -> Simply Office -> Office</span></li>
				<li><b>Virtual Packages:</b> <span class="nav">Dashboard -> Simply Office -> Virtual Package</span> </li>
				<li><b>Services:</b> <span class="nav">Dashboard -> Pages -> Services</span></li>
				<li><b>Concierge Services:</b> <span class="nav">Dashboard -> Pages -> Concierge Services</span></li>
				<li><b>Gallery:</b> <span class="nav">Dashboard -> Pages -> Gallery</span></li>
				<li><b>Contact:</b> <span class="nav">Dashboard -> Pages -> Contact</span></li>
				<li><b>Location:</b> <span class="nav">Dashboard -> Simply Office -> Location</span></li>
			</ul>
		</div>
	</div>
</div>