<?php

function create_menu() {
    add_menu_page(
        __( 'Simply Office', 'Simply Office' ),     ### Need change
        'Simply Office',        ### Need change
        'manage_options',
        'simply_office',   ### Need change
        '',
        'dashicons-admin-site',
        2
    );

    add_submenu_page(
	    'simply_office',
	    __('Client Instruction'), 
	    __('Client Instruction'), 
	    'edit_themes', 
	    'theme_cms', 
	    'client_instruction'
	);
}

add_action( 'admin_menu', 'create_menu' );

function client_instruction(){
    require_once(get_stylesheet_directory() . '/functions/client-instruction.php');
}

?>