<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>

			<div id="feature-se-mobile" class="container">
				<div class="se-flex pt-lg pb-lg">
					<div class="col">
						<?php echo wp_get_attachment_image( get_field('special_feature_image'), 'full-size' ); ?>
					</div>
					<div class="col">
						<?php the_field('special_feature_content'); ?>
					</div>
				</div>
			</div>
				
				<!-- // Service Circle CTA links -->
				<?php
					$section_1_title  = get_field('section_1_title');
				?>	
				<section id="service-highlight" class="pt-lg pb-lg">
					<div class="container">
						<?php if($section_1_title): ?>
							<h1 class="center-block"><?php echo $section_1_title; ?></h1>
						<?php endif; ?>

						<?php
						if( have_rows('section_1_boxes') ):
						    while ( have_rows('section_1_boxes') ) : the_row();
						        $image = get_sub_field('image');
						        $title = get_sub_field('title');
						        $description = get_sub_field('description');
						        $button_text = get_sub_field('button_text');
						        $button_link = get_sub_field('button_link');
						        ?>
								<div class="thumbnail text-center">
									<div class="circular--landscape">
										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
									</div>

									<div class="caption text-center">
										<?php if($title): ?>
										<h2 class="h3"><?php echo $title; ?></h2>
										<?php endif; ?>
										<?php echo $description; ?>
										<p class="mt-sm"><a href="<?php echo $button_link; ?>" class="btn btn-default btn-lg" role="button">Learn More</a></p>
									</div>
								</div>
						        <?php
						    endwhile;
						else :
						    // no rows found
						endif;
						?>
					</div>
				</section>
				
				<!-- Main Body Copy - Should include h1 -->
				<?php
					$section_2_background_image = get_field('section_2_background_image');
					$half_copy = get_field('section_2_half_copy');
					$half_image = get_field('section_2_half_image');
				?>
				<div class="bg-image-charlie" style="background-image: url('<?php echo $section_2_background_image ?>');">
					<div class="container">
						<div class="body-copy pt-lg pb-lg">				
						
							<article class="fp-copy">
								<?php echo $half_copy; ?>
							</article>

							<div class="fp-image">
								<img src="<?php echo $half_image['url']; ?>" alt="<?php echo $half_image['alt']; ?>">
							</div>

						</div>
					</div>
				</div>

				<!-- Section 3 -->
				<?php
					$section_3_content = get_field('section_3_content');
					$section_3_link = get_field('section_3_link');
				?>
				<section class="pt-lg pb-lg">
					<div class="container">
						<?php echo $section_3_content; ?>
					</div>
				</section>
				<!-- end Section 3 -->

				<!-- Section 4 -->
				<?php
					$section_4_image = get_field('section_4_image');
					$section_4_content = get_field('section_4_content');
				?>	
				<section class="bg-gray-lighter pt-lg pb-lg">
					<div class="container">
						<div class="body-copy pt-lg pb-lg">				
						
							<div class="fp-image">
								<img src="<?php echo $section_4_image['url']; ?>" alt="<?php echo $section_4_image['alt']; ?>">
							</div>
							<article class="fp-copy">
								<?php echo $section_4_content; ?>
							</article>


						</div>
					</div>
				</section>
				<!-- end Section 4 -->
			</main>
		</div>
	</div>

<?php get_footer(); ?>