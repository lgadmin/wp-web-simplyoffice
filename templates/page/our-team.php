<?php

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>
				<div class="body-copy">
					<div class="container pb-lg">
						<?php if ( have_posts() ) : ?>
							<?php while ( have_posts() ) : the_post(); ?>
								<p><?php the_content(); ?></p>
							<?php endwhile; ?>
						<?php endif ?>
					</div>
				</div>

					<section class="bg-gray-lighter pt-lg pb-lg">
						<div id="concierge-service">
							<div class="container">

				<?php 
					
					$args = array(
			            'showposts'	=> -1,
			            'post_type'		=> 'member',
			        );
			        
			        $result = new WP_Query( $args );
			        wp_reset_query();
			        
			        if ( $result->have_posts() ) : ?>
						
						<div class="members">
							<h2 class="text-center h1 mb-lg">OUR TEAM</h2>
							<ul class="flex-container">
			        	
				        	<?php while( $result->have_posts() ) : $result->the_post();
				            	$image = get_field('image');
				            	$name = get_field('name');
				            	$position = get_field('position');
				            	$phone = get_field('phone');
				            	$email = get_field('email');
			            		?>
									<li class="thumbnail">
										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
										<div class="caption text-center">
		
												<a class="h2" href="#"><?php echo $name; ?></a>
												<div><?php echo $position; ?></div>
											
											<hr>
											<ul class="list-inline">
												<?php if($phone): ?>
												<li><a href="tel:<?php echo $phone; ?>"><i class="fa fa-mobile fa-lg" aria-hidden="true"></i></a></li>
												<?php endif; ?>
												<?php if($email): ?>
												<li><a href="mailto:<?php echo $email; ?>"><i class="fa fa-envelope fa-lg" aria-hidden="true"></i></a></li>
												<?php endif; ?>
											</ul>
										</div>
									</li>
			            		<?php
				        	endwhile;
				        	?>
			        		</ul>
						</div>
			        	<?php endif; ?>

							</div>
						</div>
					</section>


			</main>
		</div>
	</div>

<?php get_footer(); ?>