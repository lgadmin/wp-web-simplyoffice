<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>

			<div class="body-copy">
				<div class="container pb-lg">
					<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<p><?php the_content(); ?></p>
						<?php endwhile; ?>
					<?php endif ?>
				</div>
			</div>

			<div class="locations">
				<div class="body-copy">
					<div class="container pb-lg">
						<?php 
							$args = array(
					            'showposts'	=> -1,
					            'post_type'		=> 'location',
					        );
					        $result = new WP_Query( $args );
					        wp_reset_query();
					        if ( $result->have_posts() ) :
					            while( $result->have_posts() ) : $result->the_post();
					        		$location = get_the_title();
					        		$buildings = get_field('buildings');
					        		//Buildings
					        		foreach ($buildings as $key => $building) {
					        			$building_id = $building->ID;
					        			$building_location = get_field('location', $building_id);
					        			$building_map = get_field('google_map', $building_id);
					        			$building_description = get_field('description', $building_id);
					        			?>
					        			<section class="building">
											<h2><?php echo $building_location['address']; ?><br>
											<?php echo $building_location['city']; ?>, <?php echo $building_location['province']; ?><br>
											<?php echo $building_location['postcode']; ?></h2>

											<div class="split-content">
												<div class="google-map">
													<?php echo $building_map; ?>
												</div>
												<div class="copy">
													<?php echo $building_description; ?>
												</div>
											</div>
										</section>
					        			<?php
					        		}
							    endwhile;
					        endif; // End Loop
					    ?>
					</div>
				</div>
			</div>

			</main>
		</div>
	</div>

<?php get_footer(); ?>