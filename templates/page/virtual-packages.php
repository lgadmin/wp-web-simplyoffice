<?php

function sort_office($a, $b)
{
	return strcmp(get_field('room_title', $a->ID), get_field('room_title', $b->ID));
}

get_header(); ?>
	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>
				
				<!-- Page Body Content -->
				<div class="body-copy">
					<div class="container pb-lg pt-lg">
						<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; // end of the loop. ?>
					</div>
				</div>

				<section class="container mb-lg virtual-packages-banner">
			    	<div class="feature-banner">
						<div class="bg-bravo">
							<div class="cta-charlie">
								<!--<p>FIRST MONTH FREE FOR ALL VIRTUAL PLANS!</p>-->
								<p>ONE TIME SET UP FEE: $99.95&nbsp;+&nbsp;GST</p>
							</div>
						</div>
					</div>
			    </section>

				<!-- Virtual Packages -->
				<div class="virtual-packages">
					<?php 
						
						$args = array(
				            'showposts'	=> -1,
				            'post_type'		=> 'virtual_package',
				        );

				        $result = new WP_Query( $args );
				        
				        wp_reset_query(); 
			        ?>
			        
			        <?php  if ( $result->have_posts() ) :?>
							<?php $i = 1; ?>
							<?php while( $result->have_posts() ) : $result->the_post(); ?>

							 	<h2><?php echo $i . ") "; ?><?php the_title(); ?></h2>
							 	<div class="vp-content"><?php the_content(); ?></div>
								<?php $i++; ?>
						    <?php endwhile; ?>

					<?php endif; ?>
			    </div>

			    <section class="container mb-lg">
			    	<p>We can accommodate virtual tenants who require access to their center to retrieve mail 24/7 365 days a year. Please contact our team to get pricing details. </p>
			    </section>

				<!-- Forms -->
				<?php if( have_rows('registration_forms', 126) ): ?>
						<div class="bg-gray-lighter">
						<div class="container pb-lg pt-lg">
						<div class="registration-forms">
							<h2 class="text-center mb-lg">REGISTRATION FORMS</h2>
							<ul>
								<?php while ( have_rows('registration_forms', 126) ) : the_row();
							        $file     = get_sub_field('file');
							        $filename = get_sub_field('filename');
							        $virtual_package = get_sub_field('virtual_package_form');
							    ?>

									<?php if($virtual_package == 1): ?>
										<li><i class="fa fa-file-pdf-o fa-2x"></i><a href="<?php echo $file; ?>"><?php echo $filename; ?></a></li>
						        	<?php endif; ?>
						        <?php endwhile; ?>
							</ul>
						</div>
						</div>
						</div>
					    <?php
					else :
					    // no rows found
					endif;

					if( have_rows('pricing_forms', 126) ): ?>
						<div class="container pb-lg pt-lg">
						<div class="pricing-forms">
							<h2 class="text-center mb-lg" >PRICING</h2>
							<ul>
							<?php
						    while ( have_rows('pricing_forms', 126) ) : the_row();
						        $file = get_sub_field('file');
						        $filename = get_sub_field('filename');
						        ?>
								<li><i class="fa fa-file-pdf-o fa-2x"></i><a href="<?php echo $file; ?>"><?php echo $filename; ?></a></li>
						        <?php
						    endwhile;
						    ?>
							</ul>
						</div>
						</div>
					    <?php
					else :
					    // no rows found
					endif;
				?>

			</main>

		</div>
	</div>

<?php get_footer(); ?>