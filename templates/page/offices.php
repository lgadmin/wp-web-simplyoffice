<?php

function sort_office($a, $b)
{

	$a1 = explode(" ", get_field('room_title', $a->ID));
	$b1 = explode(" ", get_field('room_title', $b->ID));
	
	if (!is_numeric($a1[1]) || !is_numeric($b1[1])) {
		return strcmp(get_field('room_title', $a->ID), get_field('room_title', $b->ID));
	}
    
    if ($a1[1] == $b1[1]) {
        return 0;
    }
	return ($a1[1] < $b1[1]) ? -1 : 1;
	
}

get_header(); ?>
	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>
				
				<div class="body-copy">
					<div class="container pb-lg pt-lg">
						<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; // end of the loop. ?>
					</div>
				</div>

				<div class="bg-gray-lighter">
					<div class="offices">
					<?php 
						$args = array(
				            'showposts'	=> -1,
				            'post_type'		=> 'location',
				        );
				        $result = new WP_Query( $args );
				        wp_reset_query();
				        if ( $result->have_posts() ) :
				            while( $result->have_posts() ) : $result->the_post();
				        		$location = get_the_title();
				        		$buildings = get_field('buildings');
				        		//Buildings
				        		foreach ($buildings as $key => $building) {
				        			$address = get_field('location', $building->ID);
				        			?>
				        			<section class="building">
									<h2><?php echo $location; ?> - <?php echo $building->post_title; ?> - <?php echo $address['address']; ?></h2>
									<div class="office-list">
									<?php
				        			$offices = get_field('offices', $building->ID);
				        			//Sort Offices
				        			usort($offices, "sort_office");
				        			//
				        			foreach ($offices as $key => $office) {
				        				$office_id = $office->ID;
				        				$thumbnail = get_the_post_thumbnail($office_id);
				        				$room_title = get_field('room_title', $office_id);
				        				$leased_out = get_field('leased_out', $office_id);
				        				?>
										<div class="single-office">
											<div class="thumbnail">
												<div class="img-cont">
													<?php echo $thumbnail; ?>
													<?php if($leased_out == 1): ?>
														<div class="leased-out">
															Office Leased
														</div>
													<?php endif; ?>
												</div>
											</div>
											<div class="description">

												<?php if($room_title): ?>
													<h3><?php echo $room_title; ?></h3>
												<?php endif; ?>

												<?php
												if( have_rows('room_features', $office_id) ):
													?>
													<ul class="features">
													<?php
												    while ( have_rows('room_features', $office_id) ) : the_row();
												        $feature = get_sub_field('feature');
												        ?>
														<li><?php echo $feature; ?></li>
												        <?php
												    endwhile;
												    ?>
													</ul>
												    <?php
												else :
												    // no rows found
												endif;
												?>

												<?php if(!$leased_out): ?>
												<?php
												if( have_rows('rates', $office_id) ):
													?>
													<hr>
													<ul class="rates">
													<?php
												    while ( have_rows('rates', $office_id) ) : the_row();
												        $rate = get_sub_field('rate');
												        ?>
														<li><?php echo $rate; ?></li>
												        <?php
												    endwhile;
												    ?>
													</ul>
												    <?php
												else :
												    // no rows found
												endif;
												?>
												<?php endif; ?>
											</div>

											<?php if(!$leased_out): ?>
											<a href="" class="cta btn btn-default btn-sm">Learn More</a>
											<?php endif; ?>
										</div>
				        				<?php
				        			}
				        			?>
				        			</div>
									</section>
				        			<?php
				        		}
						    endwhile;
				        endif; // End Loop
				    ?>
				    </div>
				</div>
				

				<?php
					wp_reset_query();
					$request_form = get_field('request_form');
				?>
			    <div class="offices-pop-up">
			    	<div>
			    		<div>
			    			<div class="left">
				    			<img src="" alt="">
				    			<div class="details">
				    				<p>Building: <span class="building"></span></p>
				    				<p>Room Type: <span class="room-type"></span></p>
				    			</div>
				    		</div>
				    		<div class="right">
				    			<?php echo do_shortcode($request_form); ?>
				    		</div>
			    		</div>
			    	</div>
			    </div>
			</main>

		</div>
	</div>

<?php get_footer(); ?>