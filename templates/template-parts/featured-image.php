<div class="feature-banner">
	
	<div class="image-cont">
		<?php if (has_post_thumbnail()) {
			the_post_thumbnail();
		} else {
			echo '<img src="' . get_stylesheet_directory_uri() . '/assets/dist/images/van-city-skyline.jpg' . '" alt="Vancouver night skyline">';
		} ?>
	</div>
	

	<?php if(!is_front_page()): ?>
	<div class="body-cont bg-bravo">
		<h2><?php the_title(); ?></h2>
	</div>
	<?php endif; ?>


	<?php if(is_front_page() && get_field('special-feature-enable') ) : ?>
		<div class="feature-se-cont">
			<div class="col">
				<?php echo wp_get_attachment_image( get_field('special_feature_image'), 'full-size' ); ?>
			</div>
			<div class="col">
				<?php the_field('special_feature_content'); ?>
			</div>
		</div>
	<?php endif; ?>


</div>