<div class="address-card">
	<address itemscope="" itemtype="http://schema.org/LocalBusiness">
		
		<div>

			<?php 
				$args = array(
		            'showposts'	=> -1,
		            'post_type'		=> 'location',
		        );
		        $result = new WP_Query( $args );
		        if ( $result->have_posts() ) :
		            while( $result->have_posts() ) : $result->the_post();
		        		$buildings = get_field('buildings');
		        		//Buildings
		        		foreach ($buildings as $key => $building) {
		        			$address = get_field('location', $building->ID);
		        			?>
		        			<section><strong><?php echo $building->post_title; ?></strong></section>

							<span class="card-map-marker" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
								<span itemprop="streetAddress"><?php echo $address['address']; ?></span><br>
								<span itemprop="addressLocality"><?php echo $address['city']; ?></span>, <span itemprop="addressRegion"><?php echo $address['province']; ?></span> <br>
								<span itemprop="postalCode"><?php echo $address['postcode']; ?></span><br>
							</span>
							<br>
		        			<?php
		        		}
		        	endwhile;
		        endif;
		        wp_reset_query();
	        ?>
		</div>

		<div>
			<?php $phone = get_field('company_phone', 'option'); ?>
			<span class="card-map-phone" itemprop="telephone">Email: <a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><?php echo do_shortcode('[lg-email]'); ?></a></span><br>
			<span class="card-map-phone" itemprop="telephone">Tel: <a href="tel:+1<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo format_phone(do_shortcode('[lg-phone-main]')); ?></a></span><br>
			<span class="card-map-phone" itemprop="telephone">Fax: <a href="tel:+1<?php echo do_shortcode('[lg-fax]'); ?>"><?php echo format_phone(do_shortcode('[lg-fax]')); ?></a></span><br>
			<span class="card-map-phone" itemprop="telephone">Toll Free: <a href="tel:+1<?php echo do_shortcode('[lg-fax]'); ?>">1-<?php echo format_phone(do_shortcode('[lg-phone-alt]')); ?></a></span><br>
		</div>

	</address>
</div>

