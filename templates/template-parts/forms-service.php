<!-- Forms -->
<?php if( have_rows('registration_forms') ): ?>
		<div class="bg-gray-lighter">
		<div class="container pb-lg pt-lg">
		<div class="registration-forms">
			<h2 class="text-center mb-lg">REGISTRATION FORMS</h2>
			<ul>
				<?php while ( have_rows('registration_forms') ) : the_row();
			        $file     = get_sub_field('file');
			        $online_form_url = get_sub_field('online_form_url');
			        $filename = get_sub_field('filename'); ?>

					<li>
						<?php echo $filename; ?>
						<div class="buttons">
							<?php if($file): ?>
								<a href="<?php echo $file; ?>" target="_blank"><i class="fa fa-file-pdf-o fa-2x"></i> Download PDF</a>
							<?php endif; ?>
							<?php if($online_form_url): ?>
								<a href="<?php echo $online_form_url; ?>" target="_blank"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Submit Online</a>
							<?php endif; ?>
						</div>
					</li>
		        
		        <?php endwhile; ?>
			</ul>
		</div>
		</div>
		</div>
	    <?php
	else :
	    // no rows found
	endif;

	if( have_rows('pricing_forms') ): ?>
		<div class="container pb-lg pt-lg">
		<div class="pricing-forms">
			<h2 class="text-center mb-lg" >PRICING</h2>
			<ul>
			<?php
		    while ( have_rows('pricing_forms') ) : the_row();
		        $file = get_sub_field('file');
		        $filename = get_sub_field('filename');
		        ?>
				<li><i class="fa fa-file-pdf-o fa-2x"></i><a href="<?php echo $file; ?>"><?php echo $filename; ?></a></li>
		        <?php
		    endwhile;
		    ?>
			</ul>
		</div>
		</div>
	    <?php
	else :
	    // no rows found
	endif;
?>