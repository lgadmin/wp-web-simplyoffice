<section id="utility-bar" class="bg-gray-darker">
	<div class="ub-phone">
		<a href="tel:+1<?php echo do_shortcode('[lg-phone-alt]'); ?>"> <i class="fa fa-phone" aria-hidden="true"></i> <span class="text-white"><?php echo "1-" . format_1800(do_shortcode('[lg-phone-alt]')); ?></span> </a>
		<a class="pl-lg hidden-xs" href="#"> <i class="fa fa-envelope" aria-hidden="true"></i> <span class="text-white"><?php echo do_shortcode('[lg-email]'); ?></span> </a>
	</div>
	<div class="ub-social"><?php echo do_shortcode('[lg-social-media]'); ?></div>
</section>